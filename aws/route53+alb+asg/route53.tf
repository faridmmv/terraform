data "aws_route53_zone" "my_domain" {
  name = "pingai.net."
}

resource "aws_route53_record" "www" {
  zone_id = data.aws_route53_zone.my_domain.zone_id
  name    = "pingai.net"
  type    = "A"

  alias {
    name                   = aws_lb.apache-alb.dns_name
    zone_id                = aws_lb.apache-alb.zone_id
    evaluate_target_health = true
  }
}