resource "aws_lb" "apache-alb" {
  name               = "apache-asg-lb"
  internal           = false
  load_balancer_type = "application"
  security_groups    = [aws_security_group.apache_sg_lb.id]
  subnets            = [aws_subnet.main-public-1.id, aws_subnet.main-public-2.id, aws_subnet.main-public-3.id]
}

resource "aws_lb_listener" "apache-listener" {
  load_balancer_arn = aws_lb.apache-alb.arn
  port              = "80"
  protocol          = "HTTP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.apache_tg.arn
  }
}

resource "aws_lb_target_group" "apache_tg" {
  name     = "apache-tg"
  port     = 80
  protocol = "HTTP"
  vpc_id   = aws_vpc.main.id
}
