variable "AMIS" {
  type = map(string)
  default = {
    us-east-1 = "ami-13be557e"
    us-east-2 = "ami-0f924dc71d44d23e2"
    eu-west-1 = "ami-844e0bf7"
  }
}

variable "AWS_REGION" {
  default = "us-east-2"
}
