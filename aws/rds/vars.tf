variable "allowed_security_groups" {
  default = "sg-05a0e32c80877bc63"
}

variable "allowed_cidr_blocks" {
  default = "0.0.0.0/0"
}

variable "db_pass" {
  default   = ""
  type      = string
  sensitive = true
}