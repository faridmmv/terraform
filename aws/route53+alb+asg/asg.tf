#data "aws_ami" "amazon-linux" {
#  most_recent = true
#  owners      = ["581561309979"]
#
#  filter {
#    name   = "name"
#    values = ["*Apache"]
#  }
#}


#resource "aws_launch_configuration" "apache" {
#  name_prefix     = "learn-terraform-aws-asg-"
#  image_id        = data.aws_ami.amazon-linux.id
#  instance_type   = "t2.micro"
#  user_data       = file("user-data.sh")
#  security_groups = [aws_security_group.allow-ssh-http.id]
#
#  lifecycle {
#    create_before_destroy = true
#  }
#}

resource "aws_launch_template" "apache" {
  name_prefix            = "main-apache"
  image_id               = var.AMIS[var.AWS_REGION]
  instance_type          = "t2.micro"
  user_data              = filebase64("user-data.sh")
  vpc_security_group_ids = [aws_security_group.allow-ssh-http.id]
}

resource "aws_autoscaling_policy" "apache-asp" {
  name                   = "main-apache-asp"
  policy_type            = "TargetTrackingScaling"
  cooldown               = 120
  autoscaling_group_name = aws_autoscaling_group.apache-asg.name

  target_tracking_configuration {
    predefined_metric_specification {
      predefined_metric_type = "ASGAverageCPUUtilization"
    }

    target_value = 40.0
  }
}

resource "aws_autoscaling_group" "apache-asg" {
  name                = "main-apache-asg"
  min_size            = 1
  max_size            = 3
  desired_capacity    = 1
  vpc_zone_identifier = [aws_subnet.main-public-1.id, aws_subnet.main-public-2.id, aws_subnet.main-public-3.id]
  health_check_type   = "ELB"

  launch_template {
    id      = aws_launch_template.apache.id
    version = aws_launch_template.apache.latest_version
  }

  tag {
    key                 = "From"
    value               = "Terraform"
    propagate_at_launch = true
  }
}

resource "aws_autoscaling_attachment" "apache" {
  autoscaling_group_name = aws_autoscaling_group.apache-asg.id
  lb_target_group_arn    = aws_lb_target_group.apache_tg.arn
  #  elb                    = aws_lb.apache-alb.id
}