provider "aws" {
  region = "us-east-2"

  default_tags {
    tags = {
      hashicorp-learn = "aws-asg"
    }
  }
}

data "aws_vpc" "default" {
  default = true
}

resource "aws_default_subnet" "default_az1" {
  availability_zone = "us-east-2a"

  tags = {
    Name = "Default subnet for us-east-2a"
  }
}

resource "aws_default_subnet" "default_az2" {
  availability_zone = "us-east-2b"

  tags = {
    Name = "Default subnet for us-east-2b"
  }
}

#resource "aws_db_parameter_group" "default" {
#  name   = "rds-pg"
#  family = "mysql5.7"
#
#  parameter {
#    name  = "character_set_server"
#    value = "utf8"
#  }
#
#  parameter {
#    name  = "character_set_client"
#    value = "utf8"
#  }
#}
#
#resource "aws_rds_cluster_parameter_group" "default" {
#  name        = "rds-cluster-pg"
#  family      = "aurora5.6"
#  description = "RDS default cluster parameter group"
#
#  parameter {
#    name  = "character_set_server"
#    value = "utf8"
#  }
#
#  parameter {
#    name  = "character_set_client"
#    value = "utf8"
#  }
#}

module "cluster" {
  source = "terraform-aws-modules/rds-aurora/aws"

  name           = "test-aurora-db-tf"
  engine         = "aurora-mysql"
  engine_version = "5.7.mysql_aurora.2.10.2"
  instance_class = "db.t2.small"
  instances = {
    one = {}
    two = {}
  }

  vpc_id  = data.aws_vpc.default.id
  subnets = ["subnet-0877ad206e0a401f6", "subnet-09d355be5173a708f"]

  allowed_security_groups = ["sg-05a0e32c80877bc63"]
  allowed_cidr_blocks     = ["0.0.0.0/0"]
  port                    = 3306

  publicly_accessible    = true
  create_random_password = false
  storage_encrypted      = true
  apply_immediately      = true
  monitoring_interval    = 10
  master_password        = var.db_pass

  db_parameter_group_name         = "default.aurora-mysql5.7"
  db_cluster_parameter_group_name = "default.aurora-mysql5.7"

  enabled_cloudwatch_logs_exports = ["audit"]


  tags = {
    Environment = "dev"
    Terraform   = "true"
  }
}